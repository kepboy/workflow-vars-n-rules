# workflow-vars-n-rules

Example project that demonstrates the bug that variables defined under workflow:rules cannot
be used in job:rules:if

## Getting started

If you trigger a CI build on _main_ branch the build-main job should run.
If you trigger a CI build on _dev_ branch the build-dev job should run.

It fails, regardless of which branch the build-main job always runs.
